package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

import tech.regiment.configurations.EnableRegiment;

@SpringBootApplication
@RestController
@EnableRegiment
public class RegimentTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegimentTestApplication.class, args);
	}
}
