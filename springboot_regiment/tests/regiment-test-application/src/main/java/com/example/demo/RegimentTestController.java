package com.example.demo;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.regiment.service.Regiment;

@RequestMapping("/regiment")
@RestController
public class RegimentTestController {

	@GetMapping("/custom-log")
	public String customLog() {
		Regiment.logInfo("Custom log");
		return "logs request, rsponse and custom log";
	}
	
	
	@GetMapping("/log")
	public String log() {
		return "logs request and rsponse";
	}


	@GetMapping("/custom-ex-log")
	public String customExceptionLog() {
		Regiment.logException(new RuntimeException("Custom exception"));
		return "logs request, rsponse and custom exception";
	}
	
	@GetMapping("/exception")
	public String exception() {
		throw new RuntimeException("Exception in controller");
	}
	
	@PostMapping("/post-test")
	public void postTest() {
		
	}
	
	@PutMapping("/put-test")
	public void putTest() {
	}
	
	@DeleteMapping("/delete-test")
	public void deleteTest() {
	}
	
}
