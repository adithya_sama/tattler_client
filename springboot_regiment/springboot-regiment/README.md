[![Maven Central](https://maven-badges.herokuapp.com/maven-central/tech.regiment/springboot-regiment/badge.svg)](https://maven-badges.herokuapp.com/maven-central/tech.regiment/springboot-regiment)

# Spring Boot Regiment

A Java project to integrate you spring boot application with Regiment Platform Services


## Installation 

Install Regiment support in your spring-boot project with following dependency

```xml 
    <dependency>
        <groupId>tech.regiment</groupId>
        <artifactId>springboot-regiment</artifactId>
        <version>2.0.0</version>
    </dependency>
```
## Usage

Once the dependency is injected in your spring-boot app. You need to enable the 
Regiment Logging by adding ```@EnableRegiment``` annotation on your 
spring-boot app main class 
or on any configuration classes.

#### Example

```java
@EnableRegiment
public class RegimentTestApplication {
    ...
```

**Or**

```java
@EnableRegiment
@Configuration
public class SomeConfigurationClass {
    ...
```

### Note :
If you have enabled Regiment with ```@EnableRegiment``` the you must provide the following properties
in your ```application.properties``` or ```application.yml``` file or else application will fail to start

```properties
    regiment.api-key= <your-api-key>
    regiment.secret-key=<your-secrete-key>
    regiment.server= <the-regiment-server-url>
```

Optionally you can set the metadata, which will be sent along with every log that will be 
sent to **Regiment**

```properties
    regiment.metadata.server=my server
    regiment.metadata.application= My application
    regiment.metadata.namesapce= My namespace
```

By enabling Regiment with ```@EnableRegiment``` by default the incoming and 
outgoing requests in your application will be logged to Regiment platform.

If you want you can exclude some end-points by providing those end-point in 
your ```application.properties``` or ```application.yml``` file

```properties
    regiment.exlude-endpoints=/error,/some-other-url
```

### Sending custom logs to **Regiment**

In order to send you custom logs to regiment from you spring-boot application you just need to call methods in  ```Regiment``` in your service or controller from where you want to log. 
Then call ```logMessage(*)``` 
methods or ```log*()``` messages.

```java
@RestController
public class RegimentTestController {

	@GetMapping("/custom-log")
	public String customLog() {
		Regiment.logMessage("Custom log");
		return "logs request, response and custom log";
	}
	
	...
```