/*
 * 
 */
package tech.regiment.interceptors;

import static tech.regiment.constants.RegimentConstants.LogDetailParameters.ERROR_MESSAGE;
import static tech.regiment.constants.RegimentConstants.LogDetailParameters.ERROR_TYPE;
import static tech.regiment.constants.RegimentConstants.LogDetailParameters.METHOD;
import static tech.regiment.constants.RegimentConstants.LogDetailParameters.PATH;
import static tech.regiment.constants.RegimentConstants.LogDetailParameters.PROTOCOL;
import static tech.regiment.constants.RegimentConstants.LogDetailParameters.QUERY_PARAMS;
import static tech.regiment.constants.RegimentConstants.LogDetailParameters.REMOTE_IP;
import static tech.regiment.constants.RegimentConstants.LogDetailParameters.STATUS_CODE;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import tech.regiment.constants.RegimentConstants;
import tech.regiment.service.Regiment;

/**
 * This class is responsible for intercepting all the requests in the
 * application and logs to Regiment
 */
@Component
public class RegimentInterceptor implements HandlerInterceptor, RegimentConstants {

	/** The start time. */
	private long startTime;

	/**
	 * Method is responsible for pre-processing of the request.
	 *
	 * @param request  the request
	 * @param response the response
	 * @param handler  the handler
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		startTime = System.nanoTime();
		return true;
	}

	/**
	 * Is responsible for logging the request and response status in the Regiment.
	 *
	 * @param request  the request
	 * @param response the response
	 * @param handler  the handler
	 * @param ex       the exception object
	 * @throws Exception the exception
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		double responseTime = (System.nanoTime() - startTime) / 1000_000_000.0;
		LogLevel logLevel = getLogLevel(ex);
		String logMessage = getLogMessage(request, response, ex);
		Map<String, Object> logDetails = getLogDetails(request, response, ex);
		Regiment.logMessage(logLevel, logMessage, logDetails, responseTime, request.getRequestURI());
	}

	/**
	 * Gets the log level.
	 *
	 * @param ex the exception object
	 * @return the LogLevel
	 * @see LogLevel
	 */
	private LogLevel getLogLevel(Exception ex) {
		if (ex != null)
			return LogLevel.INFO;
		else
			return LogLevel.ERROR;
	}

	/**
	 * Gets the log details for a given request and response.
	 *
	 * @param request  the request
	 * @param response the response
	 * @param ex       the exception object
	 * @return the log details
	 */
	private Map<String, Object> getLogDetails(HttpServletRequest request, HttpServletResponse response, Exception ex) {
		Map<String, Object> map = new HashMap<>();
		map.put(PROTOCOL, request.getProtocol());
		map.put(REMOTE_IP, request.getRemoteAddr());
		map.put(METHOD, request.getMethod());
		map.put(STATUS_CODE, response.getStatus());
		map.put(PATH, request.getRequestURI());
		map.put(QUERY_PARAMS, getQueryParams(request));
		if (ex != null) {
			map.put(ERROR_TYPE, ex.getClass().getName());
			map.put(ERROR_MESSAGE, ex.getMessage());
		}
		return map;
	}

	/**
	 * Gets the log message that needs to be logged to Regiment.
	 *
	 * @param request  the request
	 * @param response the response
	 * @param ex       the exception
	 * @return the log message
	 */
	private String getLogMessage(HttpServletRequest request, HttpServletResponse response, Exception ex) {
		StringBuilder logMessageBuilder = new StringBuilder();
		logMessageBuilder.append(request.getProtocol()).append(" -- ").append(new Date()).append(" ")
				.append(request.getProtocol()).append(" ").append(request.getMethod()).append(" ")
				.append(request.getRequestURI()).append(" ");
		if (ex == null) {
			logMessageBuilder.append(HttpStatus.valueOf(response.getStatus()).toString()).append(" ");
		} else {
			try (StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw)) {
				ex.printStackTrace(pw);
				logMessageBuilder.append(ex.getMessage()).append(" -- \n").append(sw.toString());
			} catch (Exception e) {
			}

		}
		return logMessageBuilder.toString();

	}

	/**
	 * Gets the query parameters that are passed as part of the request.
	 *
	 * @param request the request
	 * @return the query parameters
	 */
	private Map<String, Object> getQueryParams(HttpServletRequest request) {
		Map<String, Object> queryParams = new HashMap<>();
		String queryString;
		queryString = request.getQueryString();
		if (null != queryString) {
			String[] queries = queryString.split("&");
			for (String query : queries)
				queryParams.put(query.split("=")[0], query.split("=")[1]);
		}
		return queryParams;
	}

}
