/*
 * 
 */
package tech.regiment.configurations;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 * This is a property class. Contains all the properties if they are mentioned
 * with prefix regiment.*.
 */
@ConfigurationProperties(prefix = "regiment")
@Validated
public class RegimentConfigurationProperties {

	/** The apikey. */
	@NotNull(message = "Regiment API key is invalid")
	@NotEmpty(message = "Regiment API key can not be empty")
	private String apiKey;

	/** The secret key. */
	@NotNull(message = "Regiment Secret key is invalid")
	@NotEmpty(message = "Regiment Secret key can not be empty")
	private String secretKey;

	/** The meta data. */
	private Map<String, Object> metaData;

	/** The exclude end-points. */
	private List<String> excludeEndpoints;

	/**
	 * Gets the exclude end-points.
	 *
	 * @return the exclude end-points
	 */
	public List<String> getExludeEndpoints() {
		if (excludeEndpoints == null)
			return new ArrayList<>();
		return excludeEndpoints;
	}

	/**
	 * Sets the exclude end-points.
	 *
	 * @param excludeUrls the new exclude end-points
	 */
	public void setExludeEndpoints(List<String> excludeUrls) {
		this.excludeEndpoints = excludeUrls;
	}

	/**
	 * Gets the api key.
	 *
	 * @return the api key
	 */
	public String getApiKey() {
		return apiKey;
	}

	/**
	 * Sets the api key.
	 *
	 * @param apiKey the new api key
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * Gets the secret key.
	 *
	 * @return the secret key
	 */
	public String getSecretKey() {
		return secretKey;
	}

	/**
	 * Sets the secret key.
	 *
	 * @param secretKey the new secret key
	 */
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	/**
	 * Gets the meta data.
	 *
	 * @return the meta data
	 */
	public Map<String, Object> getMetaData() {
		return metaData;
	}

	/**
	 * Sets the meta data.
	 *
	 * @param metaData the meta data
	 */
	public void setMetaData(Map<String, Object> metaData) {
		this.metaData = metaData;
	}

}
