/*
 * 
 */
package tech.regiment.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import tech.regiment.interceptors.RegimentInterceptor;
import tech.regiment.service.Regiment;

/**
 * This class is responsible for enabling {@link RegimentInterceptor} in spring
 * spring web mvc context
 */
@EnableAsync(proxyTargetClass = true)
@ComponentScan(basePackages = "tech.regiment")
@ConfigurationPropertiesScan(basePackages = "tech.regiment")
public class RegimentMvcConfigurer implements WebMvcConfigurer {

	private RegimentInterceptor regimentInterceptor;

	
	@Autowired
	public RegimentMvcConfigurer(RegimentInterceptor regimentInterceptor,
			RegimentConfigurationProperties configurationProperties, RestTemplateBuilder restTemplateBuilder) {
		this.regimentInterceptor = regimentInterceptor;
		Regiment.initRegimentService(configurationProperties, restTemplateBuilder);
	}


	/**
	 * Registers the {@link RegimentInterceptor} instance in spring web mvc context.
	 *
	 * @param registry the registry
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(regimentInterceptor);
	}
}
