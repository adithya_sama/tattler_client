package tech.regiment.configurations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

/**
 * This is the annotation that is used to enable Regiment feature in the
 * application. If any of the configuration classes are annotated with this,
 * then Regiment will be enabled in the application.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(RegimentMvcConfigurer.class)
@Documented
public @interface EnableRegiment {

}
