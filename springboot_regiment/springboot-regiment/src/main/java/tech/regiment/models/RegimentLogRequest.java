/*
 * 
 */
package tech.regiment.models;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Represents the DTO for Regiment loggin service.
 */
@JsonPropertyOrder({ "log_message", "log_details", "error_type", "error_message", "log_type", "log_level", "metadata",
		"generated_at", "response_time" })
@JsonInclude(value = Include.NON_NULL)
public class RegimentLogRequest {

	/** The log message. */
	@JsonProperty("log_message")
	private String logMessage;

	/** The log details. */
	@JsonProperty("log_details")
	private Map<String, Object> logDetails;

	/** The log type. */
	@JsonProperty("log_type")
	private String logType;

	/** The log level. */
	@JsonProperty("log_level")
	private int logLevel;

	/** The metadata. */
	@JsonProperty("metadata")
	private Map<String, Object> metadata;

	/** The generated at. */
	@JsonProperty("generated_at")
	private Long generatedAt;

	/** The response time. */
	@JsonProperty("response_time")
	private double responseTime;

	/**
	 * Gets the log message.
	 *
	 * @return the log message
	 */
	public String getLogMessage() {
		return logMessage;
	}

	/**
	 * Sets the log message.
	 *
	 * @param logMessage the new log message
	 */
	public void setLogMessage(String logMessage) {
		this.logMessage = logMessage;
	}

	/**
	 * Gets the log details.
	 *
	 * @return the log details
	 */
	public Map<String, Object> getLogDetails() {
		return logDetails;
	}

	/**
	 * Sets the log details.
	 *
	 * @param logDetails the log details
	 */
	public void setLogDetails(Map<String, Object> logDetails) {
		this.logDetails = logDetails;
	}

	/**
	 * Gets the log type.
	 *
	 * @return the log type
	 */
	public String getLogType() {
		return logType;
	}

	/**
	 * Sets the log type.
	 *
	 * @param logType the new log type
	 */
	public void setLogType(String logType) {
		this.logType = logType;
	}

	/**
	 * Gets the log level.
	 *
	 * @return the log level
	 */
	public int getLogLevel() {
		return logLevel;
	}

	/**
	 * Sets the log level.
	 *
	 * @param logLevel the new log level
	 */
	public void setLogLevel(int logLevel) {
		this.logLevel = logLevel;
	}

	/**
	 * Gets the metadata.
	 *
	 * @return the metadata
	 */
	public Map<String, Object> getMetadata() {
		return metadata;
	}

	/**
	 * Sets the metadata.
	 *
	 * @param metadata the metadata
	 */
	public void setMetadata(Map<String, Object> metadata) {
		this.metadata = metadata;
	}

	/**
	 * Gets the generated at.
	 *
	 * @return the generated at
	 */
	public Long getGeneratedAt() {
		return generatedAt;
	}

	/**
	 * Sets the generated at.
	 *
	 * @param generatedAt the new generated at
	 */
	public void setGeneratedAt(Long generatedAt) {
		this.generatedAt = generatedAt;
	}

	/**
	 * Gets the response time.
	 *
	 * @return the response time
	 */
	public double getResponseTime() {
		return responseTime;
	}

	/**
	 * Sets the response time.
	 *
	 * @param responseTime the new response time
	 */
	public void setResponseTime(double responseTime) {
		this.responseTime = responseTime;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "RegimentLogRequest [logMessage=" + logMessage + ", logDetails=" + logDetails + ", logType=" + logType
				+ ", logLevel=" + logLevel + ", metadata=" + metadata + ", generatedAt=" + generatedAt
				+ ", responseTime=" + responseTime + "]";
	}
}
