/*
 * 
 */
package tech.regiment.errorhandling;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

/**
 * This class is responsible for handling any errors that occurs while logging
 * the messages to Regiment.
 */
public class RegimentExhangeErrorhandler implements ResponseErrorHandler {

	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(RegimentExhangeErrorhandler.class);

	/**
	 * Checks for error in the RestTemplate response.
	 *
	 * @param httpResponse the HTTP response
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
		return (httpResponse.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR
				|| httpResponse.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR);
	}

	/**
	 * If the previous method
	 * {@link RegimentExhangeErrorhandler#hasError(ClientHttpResponse)} true, then
	 * this method handles the exception.
	 *
	 * @param httpResponse the HTTP response
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void handleError(ClientHttpResponse httpResponse) throws IOException {
		log.error("Something went wrong while sending your log to regiment. Regiment responded with "
				+ httpResponse.getStatusCode());
		if (httpResponse.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR) {
			log.error("Regiment server returned with a 500 error, please check all regiment configurations.");
		} else if (httpResponse.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR) {
			if (httpResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
				log.error("The Regiment URL that you are using might not be correct");
			} else if (httpResponse.getStatusCode() == HttpStatus.UNAUTHORIZED) {
				log.error("The API key and Secrete that you are using may not be correct");
			} else {
				log.error(
						"Some client side error has happened. The Regiment server URL or The API key and Secrete that you are using may not be correct");
			}
		}

	}
}
