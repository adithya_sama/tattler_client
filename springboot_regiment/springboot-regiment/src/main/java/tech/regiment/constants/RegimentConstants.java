/*
 * 
 */
package tech.regiment.constants;

/**
 * RegimentConstants.
 */
public interface RegimentConstants {

	/**
	 * The Interface RegimentHeaders.
	 */
	static interface RegimentHeaders {

		/** The header api key. */
		String HEADER_API_KEY = "api-key";

		/** The header api secrete key. */
		String HEADER_API_SECRET_KEY = "api-secret-key";
	}

	/**
	 * The Interface LogDetailParameters.
	 */
	static interface LogDetailParameters {

		/** The Constant PROTOCOL. */
		static final String PROTOCOL = "protocol";

		/** The Constant REMOTE_IP. */
		static final String REMOTE_IP = "remote_ip";

		/** The Constant METHOD. */
		static final String METHOD = "method";

		/** The Constant STATUS_CODE. */
		static final String STATUS_CODE = "status_code";

		/** The Constant PATH. */
		static final String PATH = "path";

		/** The Constant QUERY_PARAMS. */
		static final String QUERY_PARAMS = "query_params";

		/** The Constant ERROR_TYPE. */
		static final String ERROR_TYPE = "error_type";

		/** The Constant ERROR_MESSAGE. */
		static final String ERROR_MESSAGE = "error_message";

	}

	/**
	 * The LogType corresponding to Regiment configuration.
	 */
	enum LogType {

		/** The string. */
		STRING("string");

		/** The String value corresponding to the LogLevel. */
		private String value;

		/**
		 * Instantiates a new log type.
		 *
		 * @param value the value
		 */
		LogType(String value) {
			this.value = value;
		}

		/**
		 * Gets the value.
		 *
		 * @return the value
		 */
		public String getValue() {
			return value;
		}
	}

	/**
	 * The LogLevel corresponding to Regiment configuration.
	 */
	public enum LogLevel {

		/** The debug. */
		DEBUG(10),
		/** The info. */
		INFO(20),
		/** The warning. */
		WARNING(30),
		/** The error. */
		ERROR(40),
		/** The critical. */
		CRITICAL(50), NOTSET(0);

		/** The integer value corresponding to the LogLevel. */
		private int value;

		/**
		 * Instantiates a new log level.
		 *
		 * @param value the value
		 */
		LogLevel(int value) {
			this.value = value;
		}

		/**
		 * Returns the integer value corresponding to the LogLevel.
		 *
		 * @return The integer value corresponding to the LogLevel
		 */
		public int getValue() {
			return value;
		}
	}
}
