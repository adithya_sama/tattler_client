/*
 * 
 */
package tech.regiment.service;

import static tech.regiment.constants.RegimentConstants.LogDetailParameters.ERROR_MESSAGE;
import static tech.regiment.constants.RegimentConstants.LogDetailParameters.ERROR_TYPE;
import static tech.regiment.constants.RegimentConstants.RegimentHeaders.HEADER_API_KEY;
import static tech.regiment.constants.RegimentConstants.RegimentHeaders.HEADER_API_SECRET_KEY;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tech.regiment.configurations.RegimentConfigurationProperties;
import tech.regiment.constants.RegimentConstants.LogLevel;
import tech.regiment.constants.RegimentConstants.LogType;
import tech.regiment.errorhandling.RegimentExhangeErrorhandler;
import tech.regiment.interceptors.RegimentInterceptor;
import tech.regiment.models.RegimentLogRequest;

/**
 * This class has all the methods that a user need to log a message to the
 * Regiment.
 */
public class Regiment {

	/**
	 * Private constructor to avoid instantiation of this class. Methods of this class are used statically.
	 */
	private Regiment() {
	}

	private static final ExecutorService THREAD_POOL = Executors.newCachedThreadPool();

	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(Regiment.class);

	private static final String REGIMENT_SERVER_URL = "https://ingest.regiment.tech";

	/** The configuration properties. */
	private static RegimentConfigurationProperties configurationProperties;

	/** The regiment rest template. */
	private static RestTemplate regimentRestTemplate;

	/**
	 * Initializes Regiment logging.
	 *
	 * @param configurationProperties the configuration properties
	 * @param restTemplateBuilder     the rest template builder
	 * @see RegimentExhangeErrorhandler
	 */
	public static void initRegimentService(final RegimentConfigurationProperties configurationProperties,
			final RestTemplateBuilder restTemplateBuilder) {
		Regiment.configurationProperties = configurationProperties;
		Regiment.regimentRestTemplate = restTemplateBuilder.build();
		regimentRestTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(REGIMENT_SERVER_URL));
		regimentRestTemplate.setErrorHandler(new RegimentExhangeErrorhandler());

	}

	/**
	 * This message logs the stack trace of the given exception along with the
	 * default exception message.
	 *
	 * @param message the message
	 * @param ex      the exception object
	 */
	public static void logException(String message, Exception ex) {
		Map<String, Object> logDetails = new HashMap<>();
		logDetails.put(ERROR_TYPE, ex.getClass());
		logDetails.put(ERROR_MESSAGE, message);
		logException(message, ex, logDetails);
	}

	/**
	 * This message logs the stack trace of the given exception along with the
	 * provided exception message.
	 *
	 * @param ex the exception object
	 */
	public static void logException(Exception ex) {
		String message = ex.getMessage() == null ? "" : ex.getMessage();
		Map<String, Object> logDetails = new HashMap<>();
		logDetails.put(ERROR_TYPE, ex.getClass());
		logDetails.put(ERROR_MESSAGE, message);
		logException(message, ex, logDetails);
	}

	/**
	 * This message logs the stack trace of the given exception along with the
	 * default exception message and any additional details that user provides.
	 *
	 * @param ex         the exception object
	 * @param logDetails the log details
	 */
	public static void logException(Exception ex, Map<String, Object> logDetails) {
		String message = ex.getMessage() == null ? "" : ex.getMessage();
		if (!logDetails.containsKey(ERROR_TYPE))
			logDetails.put(ERROR_TYPE, ex.getClass());
		if (!logDetails.containsKey(ERROR_MESSAGE))
			logDetails.put(ERROR_MESSAGE, message);
		logException(message, ex, logDetails);
	}

	/**
	 * This message logs the stack trace of the given exception along with the user
	 * provided exception message and any additional details that user provides.
	 *
	 * @param message    the message
	 * @param ex         the exception object
	 * @param logDetails the log details
	 */
	public static void logException(String message, Exception ex, Map<String, Object> logDetails) {
		if (!logDetails.containsKey(ERROR_TYPE))
			logDetails.put(ERROR_TYPE, ex.getClass());
		if (!logDetails.containsKey(ERROR_MESSAGE))
			logDetails.put(ERROR_MESSAGE, message);
		String logMessage = message + " -- " + getStacktraceAsString(ex);
		logMessage(LogLevel.ERROR, logMessage, logDetails, 0);
	}

	/**
	 * This message logs the user provided message with the provided
	 * {@link LogLevel}
	 *
	 * @param logLevel the log level
	 * @param message  the message
	 */
	public static void logMessage(LogLevel logLevel, String message) {
		logMessage(logLevel, message, new HashMap<>(), 0);
	}

	/**
	 * This message logs the user provided message with provided {@link LogLevel}
	 * and any additional details that user provides.
	 *
	 * @param logLevel             the log level
	 * @param message              the message
	 * @param additionalProperties the additional properties
	 */
	public static void logMessage(LogLevel logLevel, String message, Map<String, Object> additionalProperties) {
		logMessage(logLevel, message, additionalProperties, 0);
	}

	/**
	 * This message logs the user provided message. With {@link LogLevel} INFO
	 *
	 * @param message the message
	 */
	public static void logInfo(String message) {
		logMessage(LogLevel.INFO, message, new HashMap<>(), 0);
	}

	/**
	 * This message logs the user provided message with any additional details that
	 * user provides and with {@link LogLevel} INFO.
	 *
	 * @param message              the message
	 * @param additionalProperties the additional properties
	 */
	public static void logInfo(String message, Map<String, Object> additionalProperties) {
		logMessage(LogLevel.INFO, message, additionalProperties, 0);
	}

	/**
	 * This message logs the user provided message and with {@link LogLevel} ERROR.
	 *
	 * @param message the message
	 */
	public static void logError(String message) {
		logMessage(LogLevel.ERROR, message, new HashMap<>(), 0);
	}

	/**
	 * This message logs the user provided message with any additional details that
	 * user provides and with {@link LogLevel} ERROR.
	 *
	 * @param message              the message
	 * @param additionalProperties the additional properties
	 */
	public static void logError(String message, Map<String, Object> additionalProperties) {
		logMessage(LogLevel.ERROR, message, additionalProperties, 0);
	}

	/**
	 * This message logs the user provided message and with {@link LogLevel} DEBUG.
	 *
	 * @param message the message
	 */
	public static void logDebug(String message) {
		logMessage(LogLevel.DEBUG, message, new HashMap<>(), 0);
	}

	/**
	 * This message logs the user provided message with any additional details that
	 * user provides and with {@link LogLevel} DEBUG.
	 *
	 * @param message              the message
	 * @param additionalProperties the additional properties
	 */
	public static void logDebug(String message, Map<String, Object> additionalProperties) {
		logMessage(LogLevel.DEBUG, message, additionalProperties, 0);
	}

	/**
	 * This message logs the user provided message and with {@link LogLevel}
	 * CRITICAL.
	 *
	 * @param message the message
	 */
	public static void logCritical(String message) {
		logMessage(LogLevel.CRITICAL, message, new HashMap<>(), 0);
	}

	/**
	 * This message logs the user provided message with any additional details that
	 * user provides and with {@link LogLevel} CRITICAL.
	 *
	 * @param message              the message
	 * @param additionalProperties the additional properties
	 */
	public static void logCritical(String message, Map<String, Object> additionalProperties) {
		logMessage(LogLevel.CRITICAL, message, additionalProperties, 0);
	}

	/**
	 * This message logs the user provided message with provided {@link LogLevel}
	 * and any additional details that user provides. And this a special method if
	 * the excludeEndpoint is given then it doesn't logs the message. This method is
	 * user by {@link RegimentInterceptor}. Used other log message methods for your
	 * custom logs.
	 *
	 * @param logLevel             the log level
	 * @param message              the message
	 * @param additionalProperties the additional properties
	 * @param responseTime         the response time
	 * @param excludeEndPoint      the exclude endPoint
	 * 
	 * @see Regiment#logInfo(String)
	 * @see Regiment#logMessage(LogLevel, String)
	 * @see Regiment#logInfo(String, Map)
	 * @see Regiment#logMessage(LogLevel, String, Map)
	 * @see Regiment#logMessage(LogLevel, String, Map, double)
	 */
	public static void logMessage(LogLevel logLevel, String message, Map<String, Object> additionalProperties,
			double responseTime, String excludeEndPoint) {
		if (!configurationProperties.getExludeEndpoints().contains(excludeEndPoint)) {
			logMessage(logLevel, message, additionalProperties, responseTime);
		}
	}

	/**
	 * This message logs the user provided message with provided {@link LogLevel}
	 * and any additional details that user provides.
	 *
	 * @param logLevel             the log level
	 * @param message              the message
	 * @param additionalProperties the additional properties
	 * @param responseTime         the response time.. @see
	 *                             RegimentService#logMessage(String)
	 * @see Regiment#logMessage(LogLevel, String)
	 * @see Regiment#logInfo(String, Map)
	 * @see Regiment#logMessage(LogLevel, String, Map)
	 */
	public static void logMessage(LogLevel logLevel, String message, Map<String, Object> additionalProperties,
			double responseTime) {
		try {
			RegimentLogRequest logRequest = getRegimentLogRequest(logLevel, message, additionalProperties,
					responseTime);
			String regimentRequest;
			regimentRequest = new ObjectMapper().writeValueAsString(logRequest);
			log.debug("Sending log t regiment : " + regimentRequest);
			postMessage(regimentRequest);
		} catch (JsonProcessingException e) {
			log.error("Could not send log to Regiment");
		}
	}

	/**
	 * Gets the metadata for the regiment call.
	 *
	 * @return the metadata
	 */
	private static Map<String, Object> getMetadata() {
		return configurationProperties.getMetaData() != null ? configurationProperties.getMetaData() : new HashMap<>();
	}

	/**
	 * Post message to the Regiment server.
	 *
	 * @param message the message
	 * @return the response entity
	 */
	private static void postMessage(String message) {
		CompletableFuture.runAsync(() -> {
			try {
				HttpEntity<String> logRequest = new HttpEntity<>(message, getHeaders());
				log.debug("Making call to regiment with message> " + message);
				ResponseEntity<String> response = regimentRestTemplate.exchange("/log/", HttpMethod.POST, logRequest,
						String.class);
				log.debug("Successfully sent log to Regiment, Regiment responded with => " + response.getStatusCode());
			} catch (Exception e) {
				log.error("Could not send " + message + " log to Regiment");
			}
		}, THREAD_POOL);
	}

	/**
	 * Gets the regiment log request based on log message and additional details.
	 *
	 * @param logLevel     the log level
	 * @param message      the message
	 * @param logDetails   the log details
	 * @param responseTime the response time
	 * @return the regiment log request
	 */
	private static RegimentLogRequest getRegimentLogRequest(LogLevel logLevel, String message,
			Map<String, Object> logDetails, double responseTime) {
		if (logDetails == null)
			logDetails = new HashMap<>();
		RegimentLogRequest logRequest = new RegimentLogRequest();
		logRequest.setLogLevel(logLevel.getValue());
		logRequest.setLogMessage(message);
		logRequest.setLogDetails(logDetails);
		logRequest.setGeneratedAt(System.currentTimeMillis() / 1000);
		logRequest.setLogType(LogType.STRING.getValue());
		logRequest.setResponseTime(responseTime);
		logRequest.setMetadata(getMetadata());
		return logRequest;
	}

	/**
	 * Gets the HTTP headers for the Regiment log.
	 *
	 * @return the headers
	 */
	private static HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add(HEADER_API_KEY, configurationProperties.getApiKey());
		headers.add(HEADER_API_SECRET_KEY, configurationProperties.getSecretKey());
		return headers;
	}

	/**
	 * Gets the stack trace as string.
	 *
	 * @param ex the exception as object
	 * @return the stack trace as string
	 */
	private static String getStacktraceAsString(Exception ex) {
		try (StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw)) {
			ex.printStackTrace(pw);
			return sw.toString();
		} catch (Exception e) {
			return "";
		}
	}

}
