const axios = require('axios');
const moment = require('moment');

class BulletLog{

    constructor(options){
        this.metadata = options['metadata'];
        this.log_type = options['log_type'];
        this.api_key = options['api_key'];
        this.api_secret_key = options['api_secret_key'];
        this.check_type('metadata', this.metadata, [typeof({})])
        this.check_type('log_type', this.log_type, [typeof('')])
        this.check_type('api_key', this.api_key, [typeof(''), typeof(None)])
        this.check_type('api_secret_key', this.api_secret_key, [typeof(''), typeof(None)])
        this.protocol = "https"
        this.domain = "ingest.regiment.tech"
        this.log_route = "/log/"
        this.request_headers = {
            "api-key": this.api_key,
            "api-secret-key": this.api_secret_key
        }
        this.CRITICAL=50
        this.ERROR=40
        this.WARNING=30
        this.INFO=20
        this.DEBUG=10
        this.NOTSET=0
    }

    check_type = (name, val, expected_types) => {
        for (let i in expected_types){
            if (typeof(val) == expected_types[i])
                return
        }
        throw Error(`BulletLog: given ${name} of type ${typeof(val)} expected one of [${expected_types}]`);
    }

    log_info = (log_message, log_details={}, additional_attrs={}) => {
        this.log(log_message, log_details, this.INFO, additional_attrs);
    }

    log_err = (log_message, log_details={}, additional_attrs={}) => {
        this.log(log_message, log_details, this.ERROR, additional_attrs);
    }

    log_warning = (log_message, log_details={}, additional_attrs={}) => {
        this.log(log_message, log_details, this.WARNING, additional_attrs);
    }

    log_critical = (log_message, log_details={}, additional_attrs={}) => {
        this.log(log_message, log_details, this.CRITICAL, additional_attrs);
    }

    log_debug = (log_message, log_details={}, additional_attrs={}) => {
        this.log(log_message, log_details, this.DEBUG, additional_attrs);
    }

    log = async (log_message, log_details={}, log_level=10, additional_attrs={}) => {

        try {

            let request_payload = {
                "log_message": log_message,
                "log_details": log_details,
                "log_type": this.log_type,
                "log_level": log_level,
                "metadata": this.metadata,
                "generated_at": (new Date()).getTime()/1000
            }

            Object.assign(request_payload, additional_attrs);

            try{
                await axios({
                    'method': 'post',
                    'url':`${this.protocol}://${this.domain}${this.log_route}`,
                    'data': request_payload,
                    'headers': this.request_headers
                });
            }catch(err){
                console.log(`BulletLog: ${err}`);
            }

        } catch (err) {
            console.log(err);
        }
    }

    request_handler = (req, res, next) => {

        let start_time = (new Date()).getTime() / 1000;

        res.on('finish', () => {

            try {

                let log_details = {
                    "remote_ip": req.ip,
                    "protocol": req.protocol,
                    "method": req.method,
                    "status_code": res.statusCode,
                    "path": `${(req.baseUrl)?req.baseUrl:''}${req.path}`,
                    "query_params": req.query
                }

                let response_time = ((new Date()).getTime() / 1000) - start_time;
                let current_date_time = moment(new Date()).format('MM/DD/YYYY HH:mm:ss');
                let log_message = `${req.ip} - - [${current_date_time}] "${req.method} ${req.originalUrl} ${req.protocol}" ${res.statusCode} -`
                this.log_info(log_message, log_details, {
                    'response_time': response_time
                });

            } catch (err) {
                console.log(err);
            }

        });

        next();
    }

    error_handler = (err, req, res, next) => {

        try {
            let log_details = {
                "remote_ip": req.ip,
                "protocol": req.protocol,
                "method": req.method,
                "path": `${(req.baseUrl) ? req.baseUrl : ''}${req.path}`,
                "query_params": req.query,
                "error_type": err.name,
                "error_message": err.message
            }
            let current_date_time = moment(new Date()).format('MM/DD/YYYY HH:mm:ss');
            let log = `${req.ip} - - [${current_date_time}] "${req.method} ${req.originalUrl} ${req.protocol}" - (${String(err)}, ${err.stack})`
            this.log_err(log, log_details);
        }catch(err){
            console.log(err);
        }finally{
            next(err);
        }
    }

}

module.exports.BulletLog = BulletLog;
