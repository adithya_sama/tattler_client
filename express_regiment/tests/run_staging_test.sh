trap ctrl_c INT

function ctrl_c() {
    rm -rf ./express_regiment;
}

rm -rf ./express_regiment
cp -r ../express_regiment .

sed -i -e 's/domain = "ingest.regiment.tech"/domain = "ingest-staging.regiment.tech"/' ./express_regiment/bulletlog.js;

source ./.env && node ./simple_server.js;

rm -rf ./express_regiment