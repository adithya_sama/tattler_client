trap ctrl_c INT

function ctrl_c() {
    rm -rf ./express_regiment;
}

rm -rf ./express_regiment
cp -r ../express_regiment .

sed -i -e 's/protocol = "https"/protocol = "http"/; s/domain = "ingest.regiment.tech"/domain = "localhost:5002"/' ./express_regiment/bulletlog.js;

source ./.env && node ./simple_server.js;
