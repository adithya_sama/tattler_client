require('dotenv').config();

let express = require('express');

let { BulletLog } = require('./express_regiment');
console.log('bulletlog: loaded');

const bulletlog = new BulletLog({
  'metadata': {
    "environment": "staging",
    "service_name": "test_app",
    "namespace": "test_namespace"    
  },
  'log_type': 'string',
  'api_key': process.env.API_KEY,
  'api_secret_key': process.env.API_SECRET_KEY
});
console.log('bulletlog: initialized');

const app = express();

app.use(bulletlog.request_handler);
console.log('bulletlog: registered');

bulletlog.log_info('custom log from express');
console.log('bulletlog: custom log sent');

app.get('/', function (req, res) {
  res.send('Hello World');
});

app.get('/e', function (req, res) {
  a = [];
  a[0][1];
});

app.use(bulletlog.error_handler);

app.use(function onError(err, req, res, next) {
  res.statusCode = 500;
  res.end("Exception!!!");
});

console.log("running on 5001");

app.listen(5001);
