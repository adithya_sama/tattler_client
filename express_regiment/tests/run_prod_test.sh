trap ctrl_c INT

function ctrl_c() {
    rm -rf ./express_regiment;
}

rm -rf ./express_regiment
cp -r ../express_regiment .

source ./.env && node ./simple_server.js;
