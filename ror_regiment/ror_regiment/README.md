# RorRegiment

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/ror_regiment`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ror-regiment'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install ror-regiment

## Usage

```ruby
require 'ror_regiment'

options = {
  metadata: {
    type: 'test'
    # ...
  },
  log_type: 'string/json',
  api_key: 'API Key',
  api_secret_key: 'API Secret Key'
}

# Creating a logger
bulletlog = RorRegiment::BulletLog.new options

# Basic logging
bulletlog.log_debug(message, details, attributes)
bulletlog.log_info(message, details, attributes)
bulletlog.log_warning(message, details, attributes)
bulletlog.log_error(message, details, attributes)
bulletlog.log_critical(message, details, attributes)

# Using with rails
# In application.rb add the following line

config.middleware.use RorRegiment::RailsMiddleware, bulletlog

# This will handle request and error handling of http apis

```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Building

Change version in lib/ror_regiment/version.rb

Build gem and upload
```shell
gem build ./ror_regiment.gemspec
```

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/ror_regiment. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/ror_regiment/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the RorRegiment project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/ror_regiment/blob/master/CODE_OF_CONDUCT.md).
