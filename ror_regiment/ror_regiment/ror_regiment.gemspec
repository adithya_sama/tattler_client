# frozen_string_literal: true

require_relative "lib/ror_regiment/version"

Gem::Specification.new do |spec|
  spec.name = "ror-regiment"
  spec.version = RorRegiment::VERSION
  spec.authors = ["Deepak Kumar T P"]
  spec.email = ["deepak.kumar13245@gmail.com"]

  spec.summary = "regiment log connector for ruby and rails"
  spec.description = "regiment log connector for ruby and rails"
  spec.homepage = "https://bitbucket.org/adithya_sama/tattler_client/"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.4.0"

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://bitbucket.org/adithya_sama/tattler_client/"
  spec.metadata["changelog_uri"] = "https://bitbucket.org/adithya_sama/tattler_client/src/master/ror_regiment/changelog.md"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "httparty", "~> 0.17.3"

end
