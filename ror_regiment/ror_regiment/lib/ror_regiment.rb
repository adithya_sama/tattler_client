# frozen_string_literal: true

require_relative "ror_regiment/version"
require_relative "ror_regiment/bulletlog"
require_relative "ror_regiment/rails_middleware"

module RorRegiment
end
