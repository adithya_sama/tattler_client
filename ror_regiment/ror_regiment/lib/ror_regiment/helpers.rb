def check_type!(name, value, klass)
  unless value.is_a? klass
    raise StandardError.new "BulletLog: given #{name} of type #{instance_of? value} expected #{klass.to_s}"
  end
end
