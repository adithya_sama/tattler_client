require_relative 'helpers'
require_relative 'constants'
require_relative 'client'
require 'httparty'
require 'time'

module RorRegiment
  class BulletLog
    def initialize(options)
      @metadata = options[:metadata]
      @log_type = options[:log_type]
      @api_key = options[:api_key]
      @api_secret_key = options[:api_secret_key]

      check_type! 'metadata', @metadata, Hash
      check_type! 'log_type', @log_type, String
      check_type! 'api_key', @api_key, String
      check_type! 'api_secret_key', @api_secret_key, String

      @log_route = '/log/'
      options = {
        base_uri: "https://ingest.regiment.tech",
        headers: {
          'api-key': @api_key,
          'api-secret-key': @api_secret_key,
          'content-type': 'application/json'
        }
      }
      @client = Connector.new options
    end

    def log(message, details = {}, level = DEBUG, attributes = {})
      payload = {
        log_message: message,
        log_details: details,
        log_type: @log_type,
        log_level: level,
        metadata: @metadata,
        generated_at: Time.now.to_i,
      }
      payload.merge! attributes

      begin
        response = @client.class.post(@log_route, { body: payload.to_json })
        if response.response.code == '200'
          return true
        end
        puts response.parsed_response
        return false
      rescue Exception => e
        puts e
        return false
      end

    end

    def log_info(message, details = {}, attributes = {})
      log message, details, INFO, attributes
    end

    def log_error(message, details = {}, attributes = {})
      log message, details, ERROR, attributes
    end

    def log_debug(message, details = {}, attributes = {})
      log message, details, DEBUG, attributes
    end

    def log_warning(message, details = {}, attributes = {})
      log message, details, WARNING, attributes
    end

    def log_critical(message, details = {}, attributes = {})
      log message, details, CRITICAL, attributes
    end
  end
end