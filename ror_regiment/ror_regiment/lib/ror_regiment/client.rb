require 'httparty'

class Connector
  include HTTParty

  def initialize(options)
    self.class.base_uri options[:base_uri]
    self .class.headers options[:headers]
  end
end
