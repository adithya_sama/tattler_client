module RorRegiment
  class RailsMiddleware
    def initialize(app, bulletlog)
      @app = app
      @logger = bulletlog
    end

    def call(env)
      dup._call env
    end

    def _call(env)
      start_time = Time.now
      request = Rack::Request.new(env)
      details = {
        "remote_ip": request.ip,
        "protocol": env['SERVER_PROTOCOL'],
        "method": env['REQUEST_METHOD'],
        "path": "#{request.base_url}/#{request.path}",
        "query_params": request.params
      }

      begin
        @status, @headers, @response = @app.call(env)
        details[:status_code] = @status
        response_time = (Time.now - start_time).round(6)
        message = get_log_message request, details, @status
        attrs = {
          'response_time': response_time
        }
        @logger.log_info(message, details, attrs)
        [@status, @headers, @response]
      rescue StandardError => e
        response_time = (Time.now - start_time).round(6)
        message = get_log_message request, details, 500
        attrs = {
          'response_time': response_time
        }
        trace = Thread.current.backtrace.join('\n')
        message = "#{message} #{e.message} - #{trace}"
        @logger.log_error(message, details, attrs)
        raise e
      end
    end

    def get_log_message(request, details = {}, status = 200)
      current_date_time = Time.now.to_s('MM/DD/YYYY HH:mm:ss')
      "#{details[:remote_ip]} - - [#{current_date_time}] #{details[:method]} #{request.path} #{details[:protocol]} #{status} -"
    end
  end
end