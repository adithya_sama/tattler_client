class SampleController < ApplicationController
  def index
    render json: {
      hello: 'world'
    }
  end

  def error
    raise 'hello world'
  end
end
