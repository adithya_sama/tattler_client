Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/', action: :index, controller: 'sample'
  get '/error', action: :error, controller: 'sample'
end
